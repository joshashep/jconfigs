### linux

`ln -s ~/r/jconfigs/vscode/settings.json ~/.config/Code/User/settings.json`

### MacOS

`ln -s ~/r/jconfigs/vscode/settings.json ~/Library/Application\ Support/Code/User/settings.json`

### global gitignore

`git config --global core.excludesfile '~/.gitignore'`

