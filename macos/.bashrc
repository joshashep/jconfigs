alias ll="ls -laFh"
alias ga="git add"

# this may or may not collide with ghost script
alias gs="git status"

alias gc="git commit"
alias gp="git push"
alias gl="git log"

