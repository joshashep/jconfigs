#!/bin/bash

# see https://apple.stackexchange.com/questions/40734/why-is-my-host-name-wrong-at-the-terminal-prompt-when-connected-to-a-public-wifi
sudo scutil --set HostName 'jmac'
